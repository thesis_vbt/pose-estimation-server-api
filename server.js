var express = require("express");
const { execSync } = require("child_process");
var formidable = require('formidable');
const archiver = require('archiver');

const fs = require('fs')

var app = express(); app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.post("/process", async (req, response, next) => {
    console.log("Incoming OpenPose process request...")
    var form = new formidable.IncomingForm();
    form.parse(req);
    // sleep(10000);

    form.on('fileBegin', function (name, file) {
        file.path = __dirname + "/" + file.name;
    });

    form.on('file', function (name, file) {
        console.log("Received " + file.name);
        // call_openpose(file.path);
        console.log(file.path);
        console.log("Zipping output json files")

        var zipFilePath = __dirname + '/json_and_video.zip';
        try {
            fs.unlinkSync(zipFilePath)
        } catch (err) {
            console.error(err)
        }
        // create a file to stream archive data to.
        const output = fs.createWriteStream(zipFilePath);
        const archive = archiver('zip', {
            zlib: { level: 9 } // Sets the compression level.
        });

        // listen for all archive data to be written
        // 'close' event is fired only when a file descriptor is involved
        output.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('Finished preparing zipped output.');
            response.writeHead(200, {
                'Content-Type': 'application/zip',
                'Content-Length': archive.pointer()
            });

            var readStream = fs.createReadStream(zipFilePath);
            readStream.pipe(response);
        });

        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function (err) {
            throw err;
        });

        // pipe archive data to the file
        archive.pipe(output);

        archive.directory('/openpose/output/', 'json');
        archive.file('/openpose/output_video.avi');

        archive.finalize();



    })
});

function call_openpose(file_path) {
    console.log("Called openpose on " + file_path);
    execSync("rm -rf output/ && ./build/examples/openpose/openpose.bin --display 0 --net_resolution 320x176 --video " + file_path + " --write-json output/ --write_video  output_video.avi", { cwd: "../openpose" }, function (error, stdout, stderr) {
        if (error) {
            console.log('Error while calling OpenPose\n' + error.message);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
}



function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}